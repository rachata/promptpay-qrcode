const QRCode = require("qrcode")
const generatePayload = require("promptpay-qr")
const open = require("open");


const mobileNumber = "062-995-4452"
const amount = 10 
const payload = generatePayload(mobileNumber, { amount })
const option = {
    color: {
        dark: "#000000"
    }
}
QRCode.toFile("./qr.png", payload, option, function (err) {
    if (err) throw err
    open("file:///Volumes/9rachata/ice/Prompt-pay/qr.png");
    console.log("done")

})